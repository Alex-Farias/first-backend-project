#Ideia I

O projeto vai ser baseado na API pública do lolzin https://developer.riotgames.com/docs/lol#data-dragon_champions e vou fazer um Quiz das habilidades de campeões randomicos com 4 opções de entrada

--Descontinuada na segunda semana, pela complexidade da API pública do LoL

#Ideia II

O projeto vai ser baseado na API pública e encurtador de links https://www.encurtador.dev/api.html#:~:text=O%20encurtamento%20de%20Link%20%C3%A9,e%20envios%20de%20mensagens%20SMS. e vou fazer um mapeamento dos links solicitados e também um rota para retornar o link encurtado do link solicitado

##Desenvolvimento

No processo de desenvolvimento do projeto encontrei alguns impedimentos que me fizeram repensar alguns de meus conehcimentos sobre desenvolvimento de software e encontrei dificuldades de antes não haviam sido levantadas sobre serialização e deserizaliação pela parte de comunicação de APIs.

Perto da data final precisei trocar o objetivo do projeto para atender os requisitos avaliativos do desenvolvimento da matéria de Backend, entretanto também tive complicações de saúde que minaram ainda mais o pouco tempo disponível para o desenvolvimento do projeto

##Conclusão

Apesar de o código não estar da forma que imaginei inicialmente, a forma de desenvolvimento com o Spring Framework me fez perceber o quão suave pode ser a rotina de desenvolvimento de software com ferramentas mais estabelecidas na área, sempre tive dificuldades com desenvolvimento web por conta de não estar tão exposto no contexto de desenvolvimento no mercado de trabalho, porém a matéria de backend esta me ajudando a entender melhor alguns conceitos que para outros é considerado básico e também me incnetivando a procurar novas limitações de desenvolvimento. A dor de cabeça dessa vez foi conversões com objetos JSON no java, em experiências passadas foi muito tranquilo utilizar tais métodos com javascript por ser fracamente tipado, porém com java encontrei mais do que apenas uma limitação por vez enquanto desenvolvendo este projeto.

No momento que estou relatando a conclusão desse projeto ainda estou passando por complicações de saúde, porém queria testar meus limites do mesmo jeito para tentar entregar até a data limite, entretando acredito que o projeto não atenda corretamente os requisioes para a entrega e gostaria de negociar o professor @rvenson para dar continuidade ao projeto mesmo depois da entrega
