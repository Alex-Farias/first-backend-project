package com.alexfarias.firstbackendproject.Models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Component;

@Component
public class Help {
    public static JsonNode HelpResponse(){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        ObjectNode data = mapper.createObjectNode();

        data.set("estudante", mapper.valueToTree("Alex Farias de Abreu Nabo"));
        data.set("projeto", mapper.valueToTree("Encurtador de Link"));

        jsonObject.set("data", data);
        System.out.println(jsonObject);
        return jsonObject;
    }
}
