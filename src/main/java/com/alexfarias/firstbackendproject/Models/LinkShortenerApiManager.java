package com.alexfarias.firstbackendproject.Models;

import com.alexfarias.firstbackendproject.Constants.Constant;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class LinkShortenerApiManager {
    private static String getContent(String uri, String content) throws Exception  {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(content))
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public static JsonNode ShortenerResponse(String link) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        ObjectNode data = mapper.createObjectNode();

        if(link.equals("")){
            link = "O link não foi previamente mapeado pelo sistema!";
        }

        data.set("trueLink", mapper.valueToTree(link));

        jsonObject.set("data", data);
        System.out.println(jsonObject);
        return jsonObject;
    }

    public static JsonNode TrueLink(String linkShortenered){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        ObjectNode data = mapper.createObjectNode();

        String link = getContent(Constant.getRouteLinkshortener(), linkShortenered);
        data.set("linkShotenered", mapper.valueToTree(linkShortenered));

        jsonObject.set("data", data);
        System.out.println(jsonObject);
        return jsonObject;

    }
}
