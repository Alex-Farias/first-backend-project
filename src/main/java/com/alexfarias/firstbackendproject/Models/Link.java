package com.alexfarias.firstbackendproject.Models;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class Link {
    private ArrayList<String> links;

    public String getLink(String linkIndex){
        String targetExist = "";
        for (String link : getLinks()) {
            if (link.equals(linkIndex)) {
                targetExist = link;
                break;
            }
        }
        return targetExist;
    }

    public ArrayList<String> getLinks(){
        return links;
    }

    public void setLinks(String link){
        links.add(link);
    }
}
