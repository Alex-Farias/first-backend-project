package com.alexfarias.firstbackendproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstBackendProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(FirstBackendProjectApplication.class, args);
	}
}
