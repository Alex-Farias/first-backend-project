package com.alexfarias.firstbackendproject.Constants;

import org.springframework.stereotype.Component;

@Component
public class Constant {
    private final static String ROUTE_LINKSHORTENER = "https://api.encurtador.dev/encurtamentos";

    public static String getRouteLinkshortener() {
        return ROUTE_LINKSHORTENER;
    }

}
