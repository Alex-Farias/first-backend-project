package com.alexfarias.firstbackendproject.Controllers;

import com.alexfarias.firstbackendproject.Models.Help;
import com.alexfarias.firstbackendproject.Models.Link;
import com.alexfarias.firstbackendproject.Models.LinkShortenerApiManager;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstBackendProjectController {
    @Autowired
    public Link links;

    //rota ajuda
    @GetMapping("/Ajuda")
    public ResponseEntity<Object> Answer(){
        JsonNode json = Help.HelpResponse();
        return ResponseEntity.ok().body(json);
    }

    //Rota Get: Retornar o link encurtado atraves do link recebido por parametro
    @GetMapping("/Encurtar")
    public ResponseEntity<Object> Shortener(@PathVariable String link) throws Exception {
        JsonNode json = LinkShortenerApiManager.ShortenerResponse(link);
        if(!json.toString().equals("")){
            links.setLinks(json.toString());
        }
        return ResponseEntity.ok().body(json);
    }

    //Rota Post: Retornar o link não-encurtado atraves do link encurtado recebido por parametro
    @PostMapping("/LinkReal")
    public ResponseEntity<Object> TrueLink(@PathVariable String link) throws Exception {
        String trueLink = links.getLink(link);
        JsonNode json = LinkShortenerApiManager.ShortenerResponse(trueLink);
        return ResponseEntity.ok().body(json);
    }
}
